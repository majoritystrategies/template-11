<div class="slick-slider">
  <div class="slide">
   <img src="<?php the_asset_dir() ?>/slide_one.jpg" alt="" srcset="">
    <div class="slide-text">
      <h1>Best part of Waking Up</h1>
      <p>Coffee is Amazing</p>
    </div>
  </div>
  <div class="slide">
   <img src="<?php the_asset_dir() ?>/slide_two.jpg" alt="" srcset="">
    <div class="slide-text">
      <h1>Best part of Waking Up</h1>
      <p>Coffee is Amazing</p>
    </div>
  </div>
  <div class="slide">
    <img src="<?php the_asset_dir() ?>/slide_three.jpg" alt="" srcset="">
    <div class="slide-text">
      <h1>Best part of Waking Up</h1>
      <p>Coffee is Amazing</p>
    </div>
  </div>
  <div class="slide">
    <img src="<?php the_asset_dir() ?>/slide_four.jpg" alt="" srcset="">
    <div class="slide-text">
      <h1>Best part of Waking Up</h1>
      <p>Coffee is Amazing</p>
    </div>
  </div>
  <div class="slide">
    <img src="<?php the_asset_dir() ?>/slide_five.jpg" alt="" srcset="">
    <div class="slide-text">
      <h1>Best part of Waking Up</h1>
      <p>Coffee is Amazing</p>
    </div>
  </div>

</div>
